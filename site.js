var isTimerRunning = false;
var isMousePressed = false;
var isDrawing = false;

var c = document.getElementById('canvas');
var cgenField = document.getElementById('cgenField');
var timerButton = document.getElementById('timerButton');
var timer = null;
var cgen = 0;
var life = new cellular.ConwaysGame(new cellular.EndlessGridHash());
var lastDrawX = 0, lastDrawY = 0;
var lastY = lastX = 0;
var ctx = c.getContext('2d');
var offsetX = INITIAL_OFFSET_X, offsetY = INITIAL_OFFSET_Y,
    gridSize = INITIAL_CELL_SIZE; //px


function increase_cgen() {
    cgenField.innerHTML = ++cgen;
    if (cgen >= life.generations.length) life.update();
    redraw();
}


timerButton.onclick = timerButtonToggle;

function timerButtonToggle(){
    if (isTimerRunning){
        timerButton.innerHTML = "Start";
        clearInterval(timer);
    } else {
        timerButton.innerHTML = "Stop";
        timer = setInterval(increase_cgen, UPDATE_INTERVAL);
    }
    isTimerRunning = !isTimerRunning;
}


function decrease_cgen(){
    if (cgen<=0) return;
    cgenField.innerHTML = --cgen;
    redraw();
}


function drawLine(x1, y1, x2, y2, color) {
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.strokeStyle = color;
    ctx.stroke();
}

function drawGrid(){
    var w = c.width+gridSize, h = c.height+gridSize;
    for(var i=-offsetY % gridSize; i<h; i+=gridSize)
        drawLine(0, i, w, i, GRID_COLOR);
    for(var i=-offsetX % gridSize; i<w; i+=gridSize)
        drawLine(i, 0, i, h, GRID_COLOR);
}


function resize() {
    var w = window.innerWidth;
    var h = window.innerHeight;
    c.width = w;
    c.height = h;
    redraw();
}


function drawCells(grid){
    var x = 0, y = 0;
    var cellStartX = Math.floor(offsetX / gridSize);
    var cellStartY = Math.floor(offsetY / gridSize);
    var cellsX = Math.ceil(c.width / gridSize) + cellStartX + 2;
    var cellsY = Math.ceil(c.height / gridSize) + cellStartY + 2;
    var addX = (Math.abs(offsetX) % gridSize);
    var addY = (Math.abs(offsetY) % gridSize);
    if (offsetX>0 && offsetY>0) addX = gridSize-addX, addY = gridSize-addY;
    if (offsetX>0 && offsetY<0) addX = gridSize-addX;
    if (offsetX<0 && offsetY>0) addY = gridSize-addY;
    for(var i=cellStartX, x=0; i<cellsX; i++, x += gridSize) {
        for(var j=cellStartY, y=0; j<cellsY; j++, y += gridSize){
            if (grid.get(i, j)) {
                ctx.fillStyle = CELL_COLOR;
                ctx.fillRect(x-gridSize+addX, y-gridSize+addY, gridSize, gridSize); 
            }
        }
    }
}

function redraw(){
    var grid = cgen>=life.generations.length ? life.current_gen : 
        life.generations[cgen];
    ctx.clearRect(0, 0, c.width, c.height);
    drawCells(grid);
    drawGrid();
}


function put_cell(e, shouldCheckRepeatings) {
    var x = Math.floor((e.clientX + offsetX) / gridSize);
    var y = Math.floor((e.clientY + offsetY) / gridSize);
    var check = x!=lastDrawX || y!=lastDrawY;
    if (check || !shouldCheckRepeatings) { 
        cgen = life.generations.length;
        cgenField.innerHTML = cgen;
        life.current_gen.set(x, y, !life.current_gen.get(x,y));
        lastDrawX = x, lastDrawY = y;
    }
}


c.onmousedown = (e) => { 
    if (e.which==KEYCODE_MOVE) isMousePressed=true;
    if (e.which==KEYCODE_DRAW) { 
        isDrawing=true;
        put_cell(e, false);
        redraw();
    }
}

c.onmouseup = (e) => {
    if (e.which == KEYCODE_MOVE) isMousePressed = false;
    else if (e.which == KEYCODE_DRAW) isDrawing = false;
};

c.onmousemove = (e) => {
    var shouldRedraw = isMousePressed || isDrawing;
    if (isMousePressed) {
        offsetX += lastX - e.clientX;
        offsetY += lastY - e.clientY;
    }
    if (isDrawing) put_cell(e, true);
    lastX = e.clientX;
    lastY = e.clientY;
    if (shouldRedraw) redraw();
};

c.onwheel = (e) => {
    if (e.deltaY < 0 && gridSize < MAX_CELL_SIZE) {
        gridSize++;
        redraw();
    }
    else if (gridSize > MIN_CELL_SIZE) {
        gridSize--;
        redraw();
    }
};

window.addEventListener('resize', resize);
resize();
