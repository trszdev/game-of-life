cellular = {
    'interfaces' : {
        'Game' : function Game(){
            var self = this;
            self.generations = [];
            self.current_gen = null;
            self.update = ()=>{};
        },
        'Grid' : function Grid(){
            var self = this;
            self.enumerate = (callback)=>{};
            self.get = (x,y)=>{};
            self.set = (x,y,value)=>{};
            self.create_copy = ()=>{};
        },
    },

    'ConwaysGame' : function ConwaysGame(grid) {
        var self = this;
        self.generations = [];
        self.current_gen = grid;
        function get_new_cell_state(x, y) {
            var is_alive = self.current_gen.get(x,y);
            var neighbors = self.current_gen.neighbors(x,y);
            if (neighbors<2 || neighbors>3) 
                return 0;
            else if (self.current_gen.get(x,y)) 
                return 1;
            else if (!self.current_gen.get(x,y) && neighbors==3) 
                return 1;
        }
        self.update = () => {
            var cgen = self.current_gen;
            self.generations.push(cgen);
            var next_gen = cgen.create_copy();
            var new_map = {};
            cgen.enumerate((cell) => { 
                var x = cell[0], y = cell[1];
                for(var dx=-1;dx<=1;dx++)
                    for(var dy=-1;dy<=1;dy++) 
                        new_map[[x+dx, y+dy]] = 1;
            });
            for(var xy in new_map) {
                var xyparts = xy.split(',');
                var x = xyparts[0]-0, y = xyparts[1]-0;
                next_gen.set(x,y, get_new_cell_state(x,y));
            }
            self.current_gen = next_gen;
        };
    },

    'EndlessGridHash' : function EndlessGridHash(){
        var self = this;
        var map = {};
        self.get = (x,y) => !!map[[x, y]];
        self.set = (x,y,value) => map[[x, y]] = !!value;
        self.enumerate = (callback) => {
            for(var xy in map) {
                var xyparts = xy.split(',').map(a=>a-0);
                var is_alive = map[xy];
                if (!is_alive) continue;
                xyparts.push(is_alive);
                callback(xyparts);
            }
        };
        self.neighbors = (x,y) => {
            var neighbors = 0;
            for(var dx=-1;dx<=1;dx++)
                for(var dy=-1;dy<=1;dy++) 
                    if (dx!=0 || dy!=0) neighbors += self.get(x+dx,y+dy);
            return neighbors;
        };
        self.create_copy = () => {
            var copy = new EndlessGridHash();
            self.enumerate(x => copy.set.apply(null, x));
            return copy;
        };
    },

    
};
